class style:
    font = 'Monaco'
    clock_format = ': %a %d %b : %H:%M'
    fontsize = 14
    icon_size = 14
    border_width = 2

    class color:
        black = '#000000'
        blue = '#0f75bd'
        bright_blue = '#0023bc'
        grey = '#323036'
        red = '#db2d20'
        brown = '#1b1918'


layout_defaults = {
    'margin': 5,
    'border_width': style.border_width,
    'border_normal': style.color.grey,
    'border_focus': style.color.red,
}

floating_layout_defaults = {
    'margin': 0,
    'border_width': style.border_width,
    'border_normal': style.color.grey,
    'border_focus': style.color.blue,
}

bar_defaults = {
    'size': 24,
    'background': style.color.brown,
    'font': style.font,
    'padding': 0,
    'opacity': 1.0,
}

bar_defaults_top = {
    'size': 30,
    'background': style.color.red,
    'font': style.font,
    'padding': 0,
    'opacity': 1.0,
}

widget_defaults = {
    'font': style.font,
    'fontsize': style.fontsize,
}

widget_graph_defaults = {
    'margin_y': 4,
    'border_width': 1,
    'line_width': 1,
}

widget_sep_defaults = {
    'foreground': style.color.blue,
    'linewidth': 2,
    'height_percent': 55,
    'padding': 14,
}


widget_fontawesome_defaults = {
    'fontsize': 14
}
