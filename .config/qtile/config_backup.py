# -*- coding: utf-8 -*-

import os
import subprocess

from typing import List  # noqa: F401

# Qtile imports
from libqtile.config import Key, Screen, Group, Drag, Click, Match
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
from lib.defaults import (
    style,
    layout_defaults,
    floating_layout_defaults,
    bar_defaults,
    widget_defaults,
    widget_graph_defaults,
    widget_sep_defaults,
    widget_fontawesome_defaults,
)

# --------------------- Globals Defined here ---------------------------
mod = "mod4"
barfont = "monaco"  # font for the bars
barfontsize = 12  # font size for the bars
barSize = 24

# barBg = '#111110'
barFg = "#cac6c2"


# define group names and gliphs
grp1 = "1 "
grp2 = "2 "
grp3 = "3 "
grp4 = "4 "
grp5 = "5 "
grp6 = "6 "
grp7 = "7 "
grp8 = "8 "

myterm = "st"
HOME = "/home/santosh/"


# --------------------- Key Binding ------------------------------------
keys = [
    
    # Switch between windows in current stack pane
    Key([mod], "Down", lazy.layout.down()),
    Key([mod], "Up", lazy.layout.up()),
    
    # Move windows up or down in current stack
    Key([mod, "control"], "Down", lazy.layout.shuffle_down()),
    Key([mod, "control"], "Up", lazy.layout.shuffle_up()),
    
    # Switch window focus to other pane(s) of stack
    Key([mod], "space", lazy.layout.next()),
    
    # Swap panes of split stack
    Key([mod, "shift"], "space", lazy.layout.rotate()),
    
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split()),
    Key([mod], "Return", lazy.spawn(myterm)),
    
    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout()),
    
    # Layout size controls
    Key([mod], "o", lazy.layout.grow()),
    Key([mod], "i", lazy.layout.shrink()),
    Key([mod], "u", lazy.layout.normalize()),
    Key([mod], "p", lazy.layout.maximize()),
    
    # Kill panes
    Key([mod], "w", lazy.window.kill()),
    
    # Restart Qtile
    Key([mod, "control"], "r", lazy.restart()),
    
    # Logout Qtile
    Key([mod, "control"], "q", lazy.shutdown()),
    
    # Command Runner
    Key([mod], "r", lazy.spawncmd()),
    
    # Raise Volume by 10 with keyborad volume keys
    Key([], "XF86AudioRaiseVolume", lazy.spawn("pulsemixer --change-volume +10")),

    # spotify play/pause
    Key([mod, "shift"], "F7", lazy.spawn(HOME + ".local/bin/tools/sp play")),

    # spotify next
    Key([mod, "shift"], "F6", lazy.spawn(HOME + ".local/bin/tools/sp next")),

    # spotify prev
    Key([mod, "shift"], "F5", lazy.spawn(HOME + ".local/bin/tools/sp prev")),
    
    # Lower Volume by 10 with keyborad volume keys
    Key([], "XF86AudioLowerVolume", lazy.spawn("pulsemixer --change-volume -10")),
    
    # Toggle Mute
    Key([], "XF86AudioMute", lazy.spawn("pulsemixer --toggle-mute")),
    
    # mod1 + letter of group = switch to group
    Key([mod], "1", lazy.group[grp1].toscreen()),
    Key([mod], "2", lazy.group[grp2].toscreen()),
    Key([mod], "3", lazy.group[grp3].toscreen()),
    Key([mod], "4", lazy.group[grp4].toscreen()),
    Key([mod], "5", lazy.group[grp5].toscreen()),
    Key([mod], "6", lazy.group[grp6].toscreen()),
    Key([mod], "7", lazy.group[grp7].toscreen()),
    Key([mod], "8", lazy.group[grp8].toscreen()),
    
    # mod1 + shift + letter of group = switch to & move focused window to group
    Key([mod, "shift"], "1", lazy.window.togroup(grp1)),
    Key([mod, "shift"], "2", lazy.window.togroup(grp2)),
    Key([mod, "shift"], "3", lazy.window.togroup(grp3)),
    Key([mod, "shift"], "4", lazy.window.togroup(grp4)),
    Key([mod, "shift"], "5", lazy.window.togroup(grp5)),
    Key([mod, "shift"], "6", lazy.window.togroup(grp6)),
    Key([mod, "shift"], "7", lazy.window.togroup(grp7)),
    Key([mod, "shift"], "8", lazy.window.togroup(grp8)),
    
    # Move to the screen 1/2
    Key([mod], "Left", lazy.to_screen(0)),
    Key([mod], "Right", lazy.to_screen(1)),
    
    # GUI Application Shortcuts
    Key([mod], "b", lazy.spawn("firefox")),         # Firefox
    Key([mod], "f", lazy.spawn("pcmanfm")),         # PCMamFM
    Key([mod], "s", lazy.spawn("subl")),            # Sublime-Text
    Key([mod], "F8", lazy.spawn("pavucontrol")),    # Pulse Audio Controls
]

# -------------------------assign group names ---------------------------
groups = [
    Group(
        grp1,
        matches=[
            Match(wm_class=["URxvt", "Qterminal", "Xfce4-terminal", "St"])
        ],
    ),
    Group(grp2, matches=[Match(wm_class=["Geany", "Mousepad", "Sublime_text"])]),
    Group(
        grp3,
        matches=[
            Match(
                wm_class=["Chromium-browser", "Minefield", "Firefox"], role=["browser"]
            )
        ],
    ),
    Group(grp4, matches=[Match(wm_class=["MPlayer", "vlc"])]),
    Group(grp5, matches=[Match(wm_class=["Pavucontrol", "Spotify", "spotify", " "])]),
    Group(grp6, matches=[Match(wm_class=["Thunar", "Pcmanfm"])]),
    Group(grp7, matches=[Match(wm_class=["Lutris", "Steam"])]),
    Group(grp8),
]


# ----------------------------- LAYOUTS -------------------------------
# Set layouts definition for the workspaces
# http://docs.qtile.org/en/latest/manual/config/layouts.html
layout_theme = {
    "border_width": 2,
    "margin": 5,
    "border_focus": "#d26939",
    "border_normal": "#d3ebe9",
}

layouts = [
    layout.MonadTall(**layout_defaults),
    layout.Max(**layout_defaults),
    layout.Stack(num_stacks=2, **layout_defaults),
]

widget_defaults = dict(font="monaco", fontsize=12, padding=3)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        # Bar1 on screen 0
        bottom=bar.Bar(
            [
                widget.GroupBox(font=barfont, borderwidth=1),
                widget.Sep(**widget_sep_defaults),
                widget.Prompt(font=barfont, prompt="λ : "),
                widget.WindowName(font=barfont, foreground=barFg),
                widget.Sep(**widget_sep_defaults),
                widget.CurrentLayoutIcon(scale=0.7),
                widget.CurrentLayout(font=barfont, foreground=barFg),
                widget.Sep(**widget_sep_defaults),
                widget.TextBox(" :", **widget_fontawesome_defaults),
                widget.Volume(foreground="70ff70"),
                widget.Sep(**widget_sep_defaults),
                widget.Clock(format=style.clock_format, padding=6, font=style.font),
            ],
            **bar_defaults
        )
    ),
    Screen(
        # Bar1 on screen 1
        bottom=bar.Bar(
            [
                widget.GroupBox(font=barfont, borderwidth=1),
                widget.Sep(**widget_sep_defaults),
                widget.WindowName(font=barfont, foreground=barFg),
                widget.Sep(**widget_sep_defaults),
                widget.DF(
                    partition="/",
                    font=barfont,
                    fontsize=barfontsize,
                    visible_on_warn=False,
                    foreground=barFg,
                ),
                widget.Sep(**widget_sep_defaults),
                widget.DF(
                    partition="/home",
                    font=barfont,
                    fontsize=barfontsize,
                    visible_on_warn=False,
                    foreground=barFg,
                ),
                widget.Sep(**widget_sep_defaults),
                widget.DF(
                    partition="/mnt/games",
                    font=barfont,
                    fontsize=barfontsize,
                    visible_on_warn=False,
                    foreground=barFg,
                ),
                widget.Sep(**widget_sep_defaults),
                widget.CurrentLayoutIcon(scale=0.7),
                widget.CurrentLayout(font=barfont, foreground=barFg),
                widget.Sep(**widget_sep_defaults),
                widget.Systray(),
            ],
            **bar_defaults
        ),
        top=bar.Bar(
            [
                widget.Spacer(),
                widget.Sep(**widget_sep_defaults),
                widget.Notify(),
                widget.Sep(**widget_sep_defaults),
                widget.TextBox(":", **widget_fontawesome_defaults),
                widget.CPUGraph(**widget_graph_defaults),
                widget.Sep(**widget_sep_defaults),
                widget.TextBox(":", **widget_fontawesome_defaults),
                widget.MemoryGraph(**widget_graph_defaults),
                widget.Sep(**widget_sep_defaults),
            ],
            **bar_defaults
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag(
        [mod],
        "Button1",
        lazy.window.set_position_floating(),
        start=lazy.window.get_position(),
    ),
    Drag(
        [mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()
    ),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = False
bring_front_click = False
cursor_warp = False
auto_fullscreen = True
focus_on_window_activation = "focus"

floating_layout = layout.Floating(
    float_rules=[
        {"wmclass": "confirm"},
        {"wmclass": "Pavucontrol"},
        {"wmclass": "dialog"},
        {"wmclass": "download"},
        {"wmclass": "error"},
        {"wmclass": "file_progress"},
        {"wmclass": "notification"},
        {"wmclass": "splash"},
        {"wmclass": "toolbar"},
        {"wmclass": "confirmreset"},  # gitk
        {"wmclass": "makebranch"},  # gitk
        {"wmclass": "maketag"},  # gitk
        {"wname": "branchdialog"},  # gitk
        {"wname": "pinentry"},  # GPG key password entry
        {"wmclass": "ssh-askpass"},  # ssh-askpass
    ]
)

wmname = "qtile"

# Autostart hooks
@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser("~/.config/qtile/autostart.sh")
    subprocess.call([home])
